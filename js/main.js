const choo = require('choo');
const html = require('choo/html');
const extend = require('xtend');
const loadSessions = require('./data');

const getFirstSessionByTrack = require('./utils/get-first-session');

const trackTabs = require('./views/track-tabs');
const sessionTabs = require('./views/session-tabs');
const sessionView = require('./views/session-view');

const app = choo();

// Start us up once we've loaded the sessions data
loadSessions((data) => {
  const { tracks, sessions } = data;
  const INITIAL_TRACK = tracks[0].Title;
  const INITIAL_SESSION = getFirstSessionByTrack(sessions, INITIAL_TRACK).Id;

  app.model({
    state: extend(data, {
      'TRACK_VISIBLE': INITIAL_TRACK,
      'SESSION_VISIBLE': INITIAL_SESSION
    }),
    reducers: {
      changeVisibleTrack: (data, state) => {
        return extend(state, { 'TRACK_VISIBLE': data });
      },
      changeVisibleSession: (data, state) => {
        return extend(state, { 'SESSION_VISIBLE': data });
      }
    },
    effects: {
      switchTrack: (data, state, send, done) => {
        const trackTitle = data;
        const initialSession = getFirstSessionByTrack(sessions, trackTitle).Id;

        send('changeVisibleTrack', trackTitle, done);
        send('changeVisibleSession', initialSession, done);
      }
    }
  });

  const mainView = (state, prev, send) => {
    return html`
      <section class="row">
        ${trackTabs(state, prev, send)}

        <div class="track-view layout-container">
          <div class="layout-sidebar">
            <h2 class="session-tabs-heading">${state.TRACK_VISIBLE}</h2>
            ${sessionTabs(state, prev, send)}
          </div>
          ${sessionView(state, prev)}
        </div>
      </section>
    `;
  };

  app.router((route) => [
    route('/', mainView)
  ])

  const tree = app.start();
  document.querySelector('main').appendChild(tree);
});

