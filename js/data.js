const uniqBy = require('lodash/uniqBy');

function loadSessions(callback) {   
  const xobj = new XMLHttpRequest();

  xobj.overrideMimeType("application/json");
  xobj.open('GET', '../_materials/sessions.json', true);
  xobj.onreadystatechange = function () {
    if (xobj.readyState == 4 && xobj.status == "200") {
      const json = JSON.parse(xobj.responseText);
      callback(formatData(json));
    }
  };
  xobj.send(null);
}

function formatData(data) {
  const tracks = getTracks(data);
  const sessions = getSessions(data);

  return { tracks, sessions };
}

function getTracks(data) {
  let array = [];

  data['Items'].forEach((object) => {
    const { Track } = object;

    array.push(Track);
  });

  return uniqBy(array, 'Title');
}

function getSessions(data) {
  return data['Items'];
}

module.exports = loadSessions;
