const html = require('choo/html');

const trackTabs = (state, prev, send) => {
  return html`
    <nav class="track-tabs">
      ${state.tracks.map((track, index) => {
        let classString = 'track-tab';

        if(track.Title == state.TRACK_VISIBLE) {
          classString += ' active';
        }

        return html`
          <button class="${classString}" onclick=${(e) => { 
            send('switchTrack', track.Title);
          }}>
            ${track.Title}
          </button>
        `;
      })}
  `;
};

module.exports = trackTabs;
