const html = require('choo/html');
const filterSessionsByTrack = require('../utils/filter-sessions');
const formatSpeakerString = require('../utils/format-speaker');

const sessionTabs = (state, prev, send) => {
  const sessions = filterSessionsByTrack(state.sessions, state.TRACK_VISIBLE);

  return html`
    <ul class="session-tabs">
      ${sessions.map((session, index) => {
        let classString = 'session-tab';

        if(session.Id == state.SESSION_VISIBLE) {
          classString += ' active';
        }

        return html`
          <li class="${classString}" id="${session.Id}" onclick=${
            (e) => send('changeVisibleSession', session.Id)
          }>
            <header class="session-tab-title">${session.Title}</header>
            <cite class="session-tab-speakers">
              ${session.Speakers && formatSpeakerString(session.Speakers)}
            </cite>
          </li>
        `;
      })}
    </ul>
  `;
};

module.exports = sessionTabs;
