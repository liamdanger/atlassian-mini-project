const html = require('choo/html');

const getSessionById = require('../utils/get-session');
const formatSpeakerString = require('../utils/format-speaker');

const sessionView = (state, prev) => {
  const session = getSessionById(state.sessions, state.SESSION_VISIBLE);

  return html`
    <article class="session-view layout-main">
      <header>
        <h3 class="session-view-title">${session.Title}</h3>
        <p class="session-view-speakers">
          ${session.Speakers && formatSpeakerString(session.Speakers)}
        </p>
      </header>

      <p>${session.Description}</p>

      <aside role="contentinfo">
        <h4 class="session-view-subheading">About the speaker</h4>
        <p>${session.Speakers && formatSpeakerString(session.Speakers)}</p>
        ${session.Speakers && session.Speakers.map((speaker) => {
          return html`<p>${speaker.Biography}</p>`;
        })}
      </aside>
    </article>
  `;
};

module.exports = sessionView;
