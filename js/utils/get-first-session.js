const find = require('lodash/find');

function getFirstSessionByTrack(sessions, trackTitle) {
  return find(sessions, (obj) => obj.Track.Title == trackTitle);
}

module.exports = getFirstSessionByTrack;
