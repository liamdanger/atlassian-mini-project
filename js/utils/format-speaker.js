function formatSpeakerString(speakers) {
  return speakers.map((speaker) => {
    return `${speaker.FirstName} ${speaker.LastName}, ${speaker.Company}`;
  }).join(', ');
}

module.exports = formatSpeakerString;
