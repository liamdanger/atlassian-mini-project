const find = require('lodash/find');

function getSessionById(sessions, id) {
  return find(sessions, (obj) => obj.Id == id);
}

module.exports = getSessionById;
