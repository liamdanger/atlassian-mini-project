function filterSessionsByTrack(sessions, trackTitle) {
  return sessions.filter((session) => {
    return session.Track.Title == trackTitle;
  });
}

module.exports = filterSessionsByTrack;
