'use strict';

var gulp       = require('gulp');
var sass       = require('gulp-sass');
var browserify = require('browserify');
var source     = require('vinyl-source-stream');
var buffer     = require('vinyl-buffer');
var connect    = require('gulp-connect');
var es2040     = require('es2040');

var DEST_DIR = './public/';

// Run a server and livereload on change
gulp.task('connect', function() {
  connect.server({
    livereload: true
  });
});

// Bundle JS
gulp.task('scripts', function() {
  var b = browserify({
    entries: './js/main.js',
    debug: true,
    transform: [es2040]
  });

  return b.bundle()
    .pipe(source('app.js'))
    .pipe(buffer())
    .pipe(gulp.dest(DEST_DIR))
    .pipe(connect.reload());
});
gulp.task('scripts:watch', function() {
  gulp.watch('./js/**/*.js', ['scripts']);
});

// Compile Sass into CSS
gulp.task('sass', function() {
  gulp.src('./scss/*.scss')
    .pipe(sass({
      outputStyle: 'compressed'
    }).on('error', sass.logError))
    .pipe(gulp.dest(DEST_DIR))
    .pipe(connect.reload())
});
gulp.task('sass:watch', function() {
  gulp.watch('./scss/**/*.scss', ['sass']);
});

gulp.task('default', ['connect', 'sass:watch', 'scripts:watch']);
